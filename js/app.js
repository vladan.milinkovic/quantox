/* eslint-env browser */
(function() {


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showApp,error);
    } else {
        document.getElementByTagName('body').innerHTML = 'Geolocation is not supported by this browser.'
    }

    function error() {
        document.getElementById('app').style.display = 'none';
        document.getElementById('error').style.display = 'block';
    }

    function showApp(userLocation) {
        var lat,
            lng,
            appendeddatahtml,
            str,
            newstr,
            rating,
            icon,
            address,
            distance;

        lat = userLocation.coords.latitude;
        lng = userLocation.coords.longitude;
        document.getElementById('app').style.display = 'block';
        getVenues();

        function today() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd='0'+dd;
            }
            if (mm < 10) {
                mm='0'+mm;
            }

            today = ""+yyyy+""+dd+""+mm+"";

            return today;
        }

        function reqListener () {
           // document.querySelector('.loaderWrapper').style.display = 'none';
        }

        var coffeeShopItems = [];

        function getVenues() {
            var xhr = new XMLHttpRequest();
            xhr.addEventListener('load', reqListener);
            xhr.open('GET', "https://api.foursquare.com/v2/venues/search?ll="+lat+","+lng+"&query=coffee&venuePhotos=1&radius=1000&limit=10&v="+today()+"&m=foursquare&client_id=HL410M0BPO0QZXXJD3FZEKKRWCNVVTNHVBXU0BXXKV0GV2AU&client_secret=IDHYMBSMES3S4RIZ4EXDM0MRNRZIF3MNTKSVYREFE3XN0BOE");
            xhr.onload = function() {
                if (xhr.status === 200) {
                    var data = xhr.responseText;
                    var jsonResponse = JSON.parse(data);
                    document.querySelector('#venues').style.display = 'block';
                    var dataArray = jsonResponse.response.venues;
                    document.querySelector('#venues').innerHTML = '';



                    for (var i = 0; i < 10; i++){
                        coffeeShopItems.push(dataArray[i]);
                        var coffeeShop = dataArray[i];

                        if (coffeeShop.categories[0]) {
                          str = coffeeShop.categories[0].icon.prefix;
                          newstr = str.substring(0, str.length - 1);
                          icon = newstr+coffeeShop.categories[0].icon.suffix;
                        } else {
                          icon = "";
                        }

                        if (coffeeShop.location.address) {
                            address = '<p class="subinfo">'+coffeeShop.location.address+'<br>';
                        } else {
                            address = "";
                        }

                        if (coffeeShop.categories[0].shortName) {
                            name = '<span class="rating">'+coffeeShop.categories[0].shortName+'</span>';
                        }

                        if (coffeeShop.location.distance) {
                            distance = coffeeShop.location.distance
                        }

                        //
                        //
                        //
                        appendeddatahtml = '<div class=" venue col-md-6 col-sm-6 col-xs-12"><h5><span>'+coffeeShop.name+'</span></h5><p>'+address+'</p><p>Distance: '+distance+' m</p><img src="'+icon+'" alt="image forbidden 403"></div>';
                        document.querySelector('#venues').innerHTML += appendeddatahtml;


                    }

                }
                else {
                    alert('Request failed.  Returned status of ' + xhr.status);
                }
            };
            xhr.send();
        }

        document.getElementById('distance').addEventListener("click", function(){
            sortResults(1);
        });


        function sortResults(distance, price) {
            var dataEval = eval(coffeeShopItems);


            if (distance) {
                dataEval.sort(function(a,b){
                    console.log(a);

                    if(a.location.distance == b.location.distance)
                        return 0;
                    if(a.location.distance < b.location.distance)
                        return -1;
                    if(a.location.distance > b.location.distance)
                        return 1;
                });
            }


            document.querySelector('#venues').innerHTML = '';

            for (var i = 0; i < coffeeShopItems.length; i++){
                var coffeeShop = coffeeShopItems[i];

                if (coffeeShop.location.address) {
                    address = '<p class="subinfo">'+coffeeShop.location.address+'<br>';
                } else {
                    address = "";
                }

                if (coffeeShop.categories[0].shortName) {
                    name = '<span class="rating">'+coffeeShop.categories[0].shortName+'</span>';
                }

                if (coffeeShop.location.distance) {
                    distance = coffeeShop.location.distance
                }

                appendeddatahtml = '<div class=" venue col-md-6 col-sm-6 col-xs-12"><h5><span>'+coffeeShop.name+'</span></h5><p>'+address+'</p><p>Distance: '+distance+' m</p></div>';
                document.querySelector('#venues').innerHTML += appendeddatahtml;
            }
        }

    }
})();
